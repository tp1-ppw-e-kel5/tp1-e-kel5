from django import forms

class Add_Forms(forms.Form):
	error_messages = {
		'required': 'Mohon diisi form ini',
		'invalid': 'Isi input dengan URL',
	}
	attrs_name = {
		'class': 'form-control',
		'placeholder': 'Enter your friend(s) name here'
	}
	attrs_url = {
		'class': 'form-control',
		'placeholder': 'Enter your friend(s) URL here'
	}

	name = forms.CharField(label='Nama', required=True, max_length=30, widget=forms.TextInput(attrs=attrs_name))
	url = forms.URLField(label='URL', required=True, widget=forms.URLInput(attrs=attrs_url))
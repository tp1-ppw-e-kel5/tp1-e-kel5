from django.shortcuts import render
from datetime import datetime, date
from .models import Profiledata

# Create your views here.
profile_name = 'Hepzibah Smith' #TODO implement your name here
birth_date = date(1998,1,1) #TODO implement your birthday
birthdate = birth_date.strftime('%d %B %Y')
gender = 'Female' #TODO implement your gender
email = 'hello@smith.com' #TODO implement your email
desc_profile = "Antique Expert. Experience as marketer for 10 years"
expert = "Marketer || Collector || Public Speaking"
picture = "https://files.tofugu.com/articles/japanese/2016-04-25-chibi/chibihowto-03.jpg"
profil = Profiledata(name = profile_name, Birthday = birthdate, Gender= gender, Email = email, Description=desc_profile, expertise=expert, profil_pict= picture)

def index (request):
	response= {'name': profil.name, 'birthyear': profil.Birthday, 'gender': profil.Gender, 'expertise': profil.expertise, 'description': profil.Description, 'email': profil.Email, 'picture_user': profil.profil_pict}
	return render(request, 'profile_page.html', response)
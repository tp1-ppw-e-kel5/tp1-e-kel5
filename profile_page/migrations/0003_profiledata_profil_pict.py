# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-12 07:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profile_page', '0002_auto_20171012_1311'),
    ]

    operations = [
        migrations.AddField(
            model_name='profiledata',
            name='profil_pict',
            field=models.URLField(default='https://files.tofugu.com/articles/japanese/2016-04-25-chibi/chibihowto-03.jpg'),
        ),
    ]

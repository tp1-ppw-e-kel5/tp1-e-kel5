from django.shortcuts import render
from .models import StatusModel
from .forms import Status_Form
from Stats.views import profile_name

# Create your views here.
response = {}
#latest=history.last() 
def index(request):
    form = Status_Form(request.POST)
    html = 'update-status.html'
    if request.method == 'POST':
        if form.is_valid():
            response['name']=profile_name
            response['status'] = form.cleaned_data.get('status')
            post = StatusModel(status=response['status'])
            post.save()
            history = StatusModel.objects.all().order_by('date').reverse()
            form = Status_Form()
            response['form']=form
            response['history']=history
            #response['latest']= latest
            #response['name']=name
            return render(request,html,response)

    else:
        response['name']=profile_name
        form = Status_Form()
        response['form'] = form
        history = StatusModel.objects.all().order_by('date').reverse()
        response['history']=history
        #response['latest']= latest
        return render(request, html, response)
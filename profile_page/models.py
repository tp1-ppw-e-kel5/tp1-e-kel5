from django.db import models

class Profiledata(models.Model):
	name = models.CharField(max_length=250)
	Birthday = models.CharField(max_length=20)
	Gender = models.CharField(max_length=200)
	expertise = models.TextField(max_length=200)
	Description = models.CharField(max_length=200)
	Email = models.CharField(max_length=200)
	profil_pict = models.URLField(default="https://files.tofugu.com/articles/japanese/2016-04-25-chibi/chibihowto-03.jpg")
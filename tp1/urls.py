"""Lab1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import RedirectView
import profile_page.urls as profile_page
import update_status.urls as update_status
import app_friends.urls as app_friends
import Stats.urls as Stats

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^update-status/', include(update_status,namespace='update-status')),
    url(r'^friends/', include(app_friends,namespace='friends')),
    url(r'^$', RedirectView.as_view(url="/update-status/",permanent="true"), name='redirect_landing_page'),
    url(r'^profile-page/', include(profile_page,namespace='profile-page')),
    url(r'^Stats/', include(Stats,namespace='Stats')),
]

from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Add_Forms
from .models import Friends

# Create your views here.
response = {'author': "KELOMPOK 5 PPW-E"} #TODO Implement yourname
def index(request):
	friends = Friends.objects.all()
	html = 'app_friends.html'
	#TODO Implement, isilah dengan 6 kata yang mendeskripsikan anda
	response['friends'] = friends
	response['add_forms'] = Add_Forms
	return render(request, html, response)
	
def add_friend(request):
	form = Add_Forms(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['name'] = request.POST['name']
		response['url'] = request.POST['url']
		friend = Friends(name=response['name'], url=response['url'])
		friend.save()
	return HttpResponseRedirect('/friends/')
from django.db import models

# Create your models here.

class StatusModel(models.Model):
    name = models.CharField(max_length=50,default='Hepzibah Smith')
    date = models.DateTimeField(auto_now_add=True)
    status = models.TextField()


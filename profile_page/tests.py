from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, profile_name, birthdate, gender, email, desc_profile, expert
from django.http import HttpRequest

# Create your tests here.
class ProfilUnitTest(TestCase):
	def test_profil_url_is_exist(self):
		response = Client().get('/profile-page/')
		self.assertEqual(response.status_code, 200)

	def test_profil_page_using_index_func(self):
		found = resolve('/profile-page/')
		self.assertEqual(found.func, index)

	def test_profil_page_using_profile_page_template(self):
		response = Client().get('/profile-page/')
		self.assertTemplateUsed(response, 'profile_page.html')

	def test_all_profil_is_exist(self):
		self.assertIsNotNone(profile_name)
		self.assertIsNotNone(birthdate)
		self.assertIsNotNone(gender)
		self.assertIsNotNone(email)
		self.assertIsNotNone(desc_profile)
		self.assertIsNotNone(expert)

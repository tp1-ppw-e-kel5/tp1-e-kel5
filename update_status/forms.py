from django import forms

class Status_Form(forms.Form):
    attrs = {
        'class': 'form-control',
        'placeholder': 'What is on your mind?'
    }
    status = forms.CharField(widget=forms.Textarea(attrs=attrs))


from django.db import models

class Friends(models.Model):
	name = models.CharField(max_length=27)
	url = models.URLField()
	created_date = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.name

from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .forms import Status_Form
from .models import StatusModel
import datetime

# Create your tests here.

class UpdateStatusUnitTest(TestCase):

    def test_update_status_url_is_exist(self):
        response = Client().get('/update-status/')
        self.assertEqual(response.status_code,200)

    def test_update_status_index_func(self):
        found = resolve('/update-status/')
        self.assertEqual(found.func, index)

    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'status': ''})
        self.assertFalse(form.is_valid())

    def test_root_url_is_update_status(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 301)
        self.assertRedirects(response,'/update-status/',301,200)

    def test_model_can_create_new_message(self):
        #Creating a new activity
        new_activity = StatusModel.objects.create(name='Hepzibah Smith',date=datetime.date.today(),status='This is a test')
        #Retrieving all available activity
        counting_all_available_message= StatusModel.objects.all().count()
        self.assertEqual(counting_all_available_message,1)

    def test_lab4_post_success_and_render_the_result(self):
        name = 'Hepzibah Smith'
        message = 'HaiHai'
        response = Client().post('/update-status/', {'name': name, 'date': datetime.date.today(), 'status': message})
        self.assertEqual(response.status_code, 200)
        html_response = response.content.decode('utf8')
        self.assertIn(name,html_response)
        self.assertIn(message,html_response)
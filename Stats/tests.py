from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from django.http import HttpRequest

# Create your tests here.

class StatsUnitTest(TestCase):

    def test_stats_url_is_exist(self):
        response = Client().get('/Stats/')
        self.assertEqual(response.status_code,200)

    def test_stats_index_func(self):
        found = resolve('/Stats/')
        self.assertEqual(found.func, index)

    """def test_content_is_written_correctly(self):
        #Content cannot be null
        self.assertIsNotNone(content)

        #Content is filled with 30 characters at least
        self.assertTrue(len(content) >= 30)"""

    """def test_content_is_completed(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Hello, this is '+ name +'.', html_response)
        self.assertIn(content, html_response)"""

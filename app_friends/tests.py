from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from .forms import Add_Forms
from .models import Friends

# Create your tests here.
class AppFriendsUnitTest(TestCase):
	def test_app_friends_url_is_exist(self):
		response = Client().get('/friends/')
		self.assertEqual(response.status_code, 200)

	def test_appfriends_using_index_func(self):
		found = resolve('/friends/')
		self.assertEqual(found.func, index)

	def test_model_can_add_new_friends(self):
		#Creating a new activity
		new_friend = Friends.objects.create(name='chanek',url='http://chanek.herokuapp.com')

		#Retrieving all available activity
		counting_all_available_friend= Friends.objects.all().count()
		self.assertEqual(counting_all_available_friend,1)
		self.assertEqual(str(new_friend),'chanek')

	def test_app_friends_post_fail(self):
		response = Client().post('/friends/add_friend/',{'name': 'chanek', 'url': ''})
		self.assertEqual(response.status_code, 302)

	def test_app_friends_post_success_and_render_the_result(self):
		name = 'chanek'
		url = 'http://chanek.herokuapp.com'
		response = Client().post('/friends/add_friend/', {'name': name, 'url': url})
		response = Client().get('/friends/')
		self.assertEqual(response.status_code, 200)
		html_response = response.content.decode('utf8')
		self.assertIn(name,html_response)
		self.assertIn(url,html_response)